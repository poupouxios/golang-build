package main

import (
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
)

type Badword struct {
	Name string `json:"badword"`
}

type Response struct {
	Message string `json:"Answer:"`
}

func HandleLambdaEvent(badword Badword) (Response, error) {
	// Fetch a specific badword record from the DynamoDB database.
	bw, err := getItem(badword.Name)
	if err != nil || bw == nil {
		return Response{Message: fmt.Sprintf("Badword %s not found", badword.Name)}, err
	}

	return Response{Message: fmt.Sprintf("Badword %s found: Item %+v", badword.Name, bw)}, nil
}

func main() {
	lambda.Start(HandleLambdaEvent)
}
