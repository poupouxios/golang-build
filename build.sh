#!/bin/bash

gofilename="$1"
dockerfilename="Dockerfile.scratch"

if [ -z $gofilename ]
  then
	echo "No filename specified. Please pass the filename eg. test.go pass test";
	exit;
fi

if [[ $gofilename = *".go" ]]; then
	echo "Don't pass the extension of the file. Pass only the filename";
	exit;
fi

create_dockerfile(){
  echo "FROM scratch
ADD $gofilename /" > $dockerfilename;
  echo 'CMD ["/'$gofilename'"]' >> $dockerfilename;
}

create_dockerfile;

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o $gofilename $gofilename.go

dockername="$gofilename-project"

docker build -t $dockername -f $dockerfilename .

if [ $? -eq 0 ]; then
    echo "$dockername is being build. Run is as docker run -it $dockername";
else
    echo "docker build failed for $dockername";
fi
